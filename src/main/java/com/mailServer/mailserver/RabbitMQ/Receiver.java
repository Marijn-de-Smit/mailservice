package com.mailServer.mailserver.RabbitMQ;

import com.google.gson.Gson;
import com.mailServer.mailserver.dto.TfaDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;

@Component
public class Receiver {

    @Autowired
    private JavaMailSender mailSender;

    private CountDownLatch countDownLatch = new CountDownLatch(1);

    private Gson gson = new Gson();

    public void receiveMessage(String message) {
        TfaDTO dto = gson.fromJson(message, TfaDTO.class);
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(dto.getMail());
        String confirmationUrl =  "http://localhost:4200/registrationConfirm/"+dto.getTfaToken();
        mailMessage.setSubject("Confirm registration at Winnable");
        mailMessage.setText("Please activate your account by accessing the following url: " + confirmationUrl);
        mailSender.send(mailMessage);

        countDownLatch.countDown();
    }

    public CountDownLatch getCountDownLatch() {
        return countDownLatch;
    }
}
