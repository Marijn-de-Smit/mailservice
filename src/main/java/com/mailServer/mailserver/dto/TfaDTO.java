package com.mailServer.mailserver.dto;

public class TfaDTO {

    private String mail;
    private String tfaToken;

    public TfaDTO(String mail, String tfaToken) {
        this.mail = mail;
        this.tfaToken = tfaToken;
    }

    public TfaDTO() {
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getTfaToken() {
        return tfaToken;
    }

    public void setTfaToken(String tfaToken) {
        this.tfaToken = tfaToken;
    }
}
